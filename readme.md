# run

```plain
wget -q -O - https://raw.githubusercontent.com/spring-projects/spring-boot/master/spring-boot-samples/spring-boot-sample-oauth2-client/src/main/resources/application.yml  > application.yml

cat application.yml | ./yaml2env.groovy
```

# build docker-image

```plain
docker build -t yaml2env .
```

# run as docker-container

```plain
wget -q -O - https://raw.githubusercontent.com/spring-projects/spring-boot/master/spring-boot-samples/spring-boot-sample-oauth2-client/src/main/resources/application.yml \
  | docker run -i --rm yaml2env
```

