#!/bin/env groovy

@Grab('org.yaml:snakeyaml:1.23')

import org.yaml.snakeyaml.Yaml;

def cli = new CliBuilder(usage: 'showdate.groovy [-p <prefix>] [-hv]')
cli.with {
    h longOpt: 'help', 'show usage'
    v longOpt: 'verbose', 'verbose output'
    p longOpt: 'prefix', argName: 'prefix', type: String, 'prefix for variables'
}
     
def options = cli.parse(args)
if (!options) { return }
if (options.h) { cli.usage();return }

String pfx = options.p ?: ""
if (options.v) println "# yaml from stdin converted to env-syntax ..."

Yaml yaml = new Yaml()
TreeMap<String, Map<String, Object>> config = yaml.loadAs(System.in, TreeMap.class)

System.out.println(toEnv(config, "_", pfx))

def fmtEnv(String key) {
    return key.toUpperCase().replaceAll('-','').replaceAll('\\.','_')
}

def toEnv(TreeMap<String, Map<String, Object>> config, String delemiter, String prefix) {
    StringBuilder sb = new StringBuilder();
    for (String key : config.keySet()) {
        sb.append(toString(key, config.get(key), delemiter, prefix));
    }
    return sb.toString();
}

def toString(String key, Map<String, Object> map, String delemiter, String prefix) {
    StringBuilder sb = new StringBuilder();
    for (String mapKey : map.keySet()) {
        if (map.get(mapKey) instanceof Map) {
            sb.append(toString(String.format("%s%s%s", fmtEnv(key), delemiter, fmtEnv(mapKey)), (Map<String, Object>) map.get(mapKey), delemiter, prefix));
        } else {
            def pfx = (prefix) ? prefix + delemiter : ""
            sb.append(String.format("%s%s%s%s=%s%n", pfx, fmtEnv(key), delemiter, fmtEnv(mapKey), map.get(mapKey).toString()));
        }
    }
    return sb.toString();
}