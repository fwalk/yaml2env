from groovy:2.5-alpine

add readme.md /
add yaml2env.groovy /

entrypoint [ "groovy", "/yaml2env.groovy" ]
